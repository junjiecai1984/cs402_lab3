.data
var1: .word 3
var2: .word 1
var3: .word -2020

.text
.globl main
main:
	lw $t0, var1($0)    # $t0 <- var1
	lw $t1, var2($0)    # $t1 <- var2
	slt $t2, $t1, $t0
	blez $t2, else

	lw $t2, var3($0)
	sw $t2, var1($0)
	sw $t2, var2($0)
	beq $0, $0, final   # goto 'final' label unconditionally

else:
	move $t2, $t0       # swap $t0 and $t1
	move $t0, $t1
	move $t1, $t2
	sw $t0, var1($0)    # save new values to their address
	sw $t1, var2($0)

final:
	li $v0, 1		    # system call #1 - print int
	lw $a0, var1($0)
	syscall				# execute
	li $v0, 1
	lw $a0, var2($0)
	syscall
	jr $ra
