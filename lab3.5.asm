.data
msg1: .asciiz "I'm far away"
msg2: .asciiz "I'm nearby"
far: .word 1

.text
.globl main
main:
	li $v0, 5             # syscall #5: read_int
	syscall
	move $t0, $v0         # the value read is in $v0
	li $v0, 5
	syscall
	move $t1, $v0
	bne $t0, $t1, near
	li $v0, 4             # syscall #4: print_string
	la $a0, msg1
	syscall
	b far                 # branch unconditionally to 'far' label
near:
	li $v0, 4
	la $a0, msg2
	syscall
	jr $ra

